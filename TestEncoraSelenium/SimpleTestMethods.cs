﻿using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;
using Xunit.Abstractions;

namespace TestEncoraSelenium
{
    public class SimpleTestMethods: IClassFixture<WebDriverFixture>
    {
        ITestOutputHelper _testOutputHelper;
        private readonly  WebDriverFixture _fixture;
        public SimpleTestMethods(WebDriverFixture fixture,ITestOutputHelper testOutputHelper) {
            _testOutputHelper = testOutputHelper;
            _fixture = fixture;
        }

        [Fact]
        public void SimpleTestOne() {
            _testOutputHelper.WriteLine("test one");
            _fixture.ChromeDriver.Navigate().GoToUrl("http://www.google.com");
            
        }

        [Fact]
        public void SimpleTestTwo()
        {
            _testOutputHelper.WriteLine("test two");
            _fixture.ChromeDriver.Navigate().GoToUrl("http://www.facebook.com");
        }
    }
}
