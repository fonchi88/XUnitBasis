﻿using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Text;

namespace TestEncoraSelenium
{
    public class WebDriverFixture : IDisposable
    {

        public ChromeDriver ChromeDriver { get; private set; }
        public WebDriverFixture() {
            ChromeDriver = new ChromeDriver();
        }
        
        public void Dispose()
        {
            ChromeDriver.Close();
            ChromeDriver.Dispose();
        }
    }
}
