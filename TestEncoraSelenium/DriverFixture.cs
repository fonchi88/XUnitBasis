﻿using OpenQA.Selenium.Chrome;
using System;
using WebDriverManager;
using WebDriverManager.DriverConfigs.Impl;

namespace TestEncoraSelenium
{
    public class DriverFixture:IDisposable
    {
        public ChromeDriver driver { get; private set; }
        public DriverFixture()
        {
            driver = new ChromeDriver();
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromMilliseconds(100);
            driver.Navigate().GoToUrl(@"https://codility-frontend-prod.s3.amazonaws.com/media/task_static/qa_csharp_search/862b0faa506b8487c25a3384cfde8af4/static/attachments/reference_page.html");
        }

        public void Dispose()
        {
            driver.Close();
            driver.Quit();
        }
    }
}
