﻿using System;
using System.Collections.Generic;
using System.Text;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using WebDriverManager;
using WebDriverManager.DriverConfigs.Impl;
using Xunit;

namespace TestEncoraSelenium
{
    public class WebDriverTestCase : IClassFixture<DriverFixture>
    {
        public DriverFixture _driverFixture;
        public IWebDriver driver;
        public WebDriverTestCase(DriverFixture fixture)
        {

            _driverFixture = fixture;
            driver = fixture.driver;
        }

    }
}
