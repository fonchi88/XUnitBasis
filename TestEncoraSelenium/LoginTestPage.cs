﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace TestEncoraSelenium
{
    public class LoginPageTest : WebDriverTestCase
    {
        public LoginPageTest(DriverFixture fixture) : base(fixture) {

        }

        [Fact]
        public void testSomething()
        {
            // Example how to use the driver:
            var element = driver.FindElement(By.Id("element-id"));
            var html = element.GetAttribute("innerHTML");
            Assert.Contains("some_tag", html);
            
        }

        [Fact]
        public void test_SearchControllersInMainPage() 
        {
            string[] expectedId = new string[] { "search-input", "search-button" };

            foreach (string id in expectedId) {
                var searchController = driver.FindElement(By.Id(id));
                var html = searchController.GetAttribute("id");
                Assert.Contains(id.ToLower(), html.ToLower());
            }
            
        }

        [Fact]
        public void test_EmptySearchQuery_Neg() {
            var searchInput = driver.FindElement(By.Id("search-input"));
            var searchButton = driver.FindElement(By.Id("search-button"));

            searchInput.Clear();
            searchButton.Click();

            var errMsg_emptyQuery = driver.FindElement(By.Id("error-empty-query"));
            Assert.Equal("Provide some query".ToLower(), errMsg_emptyQuery.Text.ToLower());

        }

        [Fact]
        public void test_QueryReturnsOneOrMoreIslands()
        {
            var searchInput = driver.FindElement(By.Id("search-input"));
            var searchButton = driver.FindElement(By.Id("search-button"));

            searchInput.Clear();
            searchInput.SendKeys("isla");
            searchButton.Click();

            var searchResultContainer = driver.FindElement(By.XPath("//ul[@id='search-results']"));
            var searchResults = searchResultContainer.FindElements(By.XPath(".//li"));

            foreach (IWebElement result in searchResults) {
                Console.WriteLine(result.Text);
            }
            Assert.True(searchResults.Count>0);
        }

        [Fact]
        public void test_QueryReturnsNoResults()
        {
            var searchInput = driver.FindElement(By.Id("search-input"));
            var searchButton = driver.FindElement(By.Id("search-button"));

            searchInput.Clear();
            searchInput.SendKeys("test");
            searchButton.Click();

            var errMsg_NoResultsFound = driver.FindElement(By.Id("error-no-results"));

            var searchResults = driver.FindElements(By.XPath("//ul[@id='search-results']/li"));
            Assert.True(searchResults.Count == 0);
            //Assert.Equal("No results".ToLower(), errMsg_NoResultsFound.Text.ToLower());
        }

        [Fact]
        public void test_SearchPortRoyal() 
        {
            var searchInput = driver.FindElement(By.Id("search-input"));
            var searchButton = driver.FindElement(By.Id("search-button"));

            searchInput.Clear();
            searchInput.SendKeys("Port Royal");
            searchButton.Click();

            var searchResultContainer = driver.FindElement(By.XPath("//ul[@id='search-results']"));
            var searchResults = searchResultContainer.FindElements(By.XPath(".//li"));

            int i = 0;
            foreach (var result in searchResults) {
                if (result.Text=="Port Royal") {
                    i++;
                }
            }

            Assert.True(i==searchResults.Count);
        }



    }
}
